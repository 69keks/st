st - simple terminal
--------------------
st is a simple terminal emulator for X which sucks less.

# Preview
## neofetch preview
![neofetch](assets/neofetch-ss.png)

## Gruvbox (hard contrast)
![colors](assets/catppuccin-ss.png)

## vim preview
![vim](assets/vim-ss.png)

# Details
* Font: "Dina TTF" and pixelsize: 13
* Patches used:
1). Alpha
2). Blinking cursor
3). Scrollback
4). Ligatures
5). NewTerm (Opens new terminal window at pwd instead of home.)
6). Bold is not bright

Credits
-------
Based on Aurélien APTEL <aurelien dot aptel at gmail dot com> bt source code.

